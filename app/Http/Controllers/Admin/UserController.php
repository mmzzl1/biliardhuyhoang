<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    function index(){
    	return view("dashboard.login");
    }

    function login(Request $request){
    	$username = $request->name;
    	$password = $request->password;
    	if (Auth::attempt(['name' => $username, 'password' => $password])) {
            return redirect('dashboard');
        }
        else
        {
        	return back()->with('error', 'Tài khoản hoặc mật khẩu sai');
        }
    }

    function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
}