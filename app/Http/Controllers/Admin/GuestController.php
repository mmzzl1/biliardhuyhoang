<?php

namespace App\Http\Controllers\Admin;
use App\Guest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class GuestController extends Controller
{
     public function index()
    {
    	$Guests = Guest::paginate(10);
    	return view('dashboard.guest', compact('Guests'));
    }

    function search(Request $request){
    	if($request->get('keyword'))
        {
            $keyword = $request->get('keyword');
            $guests = DB::table('guests')
            ->where('name', 'LIKE', "%{$keyword}%")
            ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach($guests as $guest)
            {
               $output .= '
               <li><a href="guest/'. $guest->id .'">'.$guest->name.' số điện thoại: '.$guest->phone .'</a></li>
               ';
           }
           $output .= '</ul>';
           echo $output;
       }
    }

    function addGuest(Request $request){
    	$guest = new Guest;
    	$guest->name = $request->input('name');
    	$guest->phone = $request->input('phone');
    	$guest->address= $request->input('address');
    	if ($guest->save()) {
    		return back()->with('success', 'Thêm thành công');
    	}
    	return back()->with('error', 'Thêm không thành công');
    }
}
