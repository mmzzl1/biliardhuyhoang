<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
class CategoryController extends Controller
{
    function index(){
    	$categories  = Category::paginate(10);
    	return view('dashboard.category', compact('categories'));
    }

    function addCategory(Request $request){
    	$name = $request->name;
    	$category = new Category;
    	$category->name = $name;
    	if ($category->save()) {
    		return back()->with('success', 'Thêm danh mục thành công');
    	}
    	return back()->with('error', 'Thêm danh mục không thành công');
    }

    function editCategory(Request $request , $id){
    	$name = $request->name;
    	$category = Category::where('id', '=' , $id)->first();
    	$category->name = $name;
    	if ($category->save()) {
    		return back()->with('success', 'Sửa thành công');
    	}
    	return back()->with('error', 'Sửa không thành công');
    }

    function deleteCategory(Request $request, $id){
    	$category = Category::where('id', '=' , $id)->first();		
    	if ($category->delete()) {
    		return back()->with('success', 'Xóa thành công');
    	}
    	return back()->with('error', 'Xóa không thành công');
    }
}
