<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Table;
class TableController extends Controller
{
    function index(){
    	$tables = Table::paginate(10);

    	return view('dashboard.table', compact('tables'));
    }

    function addTable(Request $request){
    	$table = new Table;

    	$table->name = $request->input('name');
    	$table->status = 0;
    	if ($table->save()) {
    		return back()->with('success', 'Thêm thành công');
    	}
    	return back()->with('error', 'Thêm không thành công');
    }

    function editTable(Request $request, $id){
    	$name = $request->name;
    	$status = $request->status;
    	$table = Table::where('id', '=' , $id)->first();
    	$table->name = $name;
    	$table->status = $status;
    	if ($table->save()) {
    		return back()->with('success', 'Sửa thành công');
    	}
    	return back()->with('error', 'Sửa không thành công');
    }

    function deleteTable(Request $request, $id){
    	$table = Table::where('id', '=' , $id)->first();		
    	if ($table->delete()) {
    		return back()->with('success', 'Xóa thành công');
    	}
    	return back()->with('error', 'Xóa không thành công');
    }
}
