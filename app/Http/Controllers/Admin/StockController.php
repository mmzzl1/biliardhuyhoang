<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Stock;
use App\Category;
use Illuminate\Support\Facades\Auth;

class StockController extends Controller
{
    function index(){
    	//$stocks = Stock::paginate(10);
    	$stocks = Stock::with('category' , 'user')->paginate(10);
    	$categories = Category::all();
    	return view('dashboard.stock', compact('stocks', 'categories'));
    }

    function addStock(Request $request){
    	$stock = new Stock;
    	$stock->name = $request->name;
    	$stock->quality = $request->quality;
    	$stock->unit_iput = $request->unit_iput;
    	$stock->cost_price = $request->cost_price;
    	dd($request->cost_price / $request->quality);
    	$stock->id_category = $request->id_cate;
    	$stock->updated_user_id = Auth::user()->id;
    	if ($stock->save()) {
    		return back()->with('success', 'Thêm thành công');
    	}
    	return back()->with('error', 'Thêm không thành công');
    }
}
