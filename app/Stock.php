<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\User;
class Stock extends Model
{
    public function category() {
    	return $this->belongsTo(Category::class, 'id_category'); // don't forget to add your full namespace
	}

	public function user() {
    	return $this->belongsTo(User::class, 'updated_user_id'); // don't forget to add your full namespace
	}
}
