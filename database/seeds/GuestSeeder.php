<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('guests')->truncate();
        DB::table('guests')->insert([
        	'name'=>'Đồng',
        	'phone'=>'025558842',
        	'address'=>'bình tân, tphcm',
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
    		'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('guests')->insert([
        	'name'=>'Hải',
        	'phone'=>'099898989',
        	'address'=>'Nhật bổn',
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
    		'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('guests')->insert([
        	'name'=>'Vũ',
        	'phone'=>'0024545454',
        	'address'=>'dĩ an, bình dương',
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
    		'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
