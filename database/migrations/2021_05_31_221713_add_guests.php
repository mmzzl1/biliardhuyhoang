<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGuests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guests', function (Blueprint $table) {
           DB::statement('ALTER TABLE guests ADD FULLTEXT `name` (`name`, `phone`)'); //đánh index cho cột name và phone
           DB::statement('ALTER TABLE guests ENGINE = MyISAM'); // đánh index theo kiểu MyISam ngoài ra còn có kiểu InnoDB nếu không có dòng này cũng được mysql sẽ mặc định là index kiểu MyISAM nhé
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('guests', function (Blueprint $table) {
           DB::statement('ALTER TABLE guests DROP INDEX name');
           DB::statement('ALTER TABLE guests DROP INDEX phone');  // khi chạy lệnh rollback thì làm điều ngược lại với thằng trên thế thôi
       });
    }
}
