@if ($message = Session::get('success'))
<div class="box">
    <div class="box-body ribbon-box">
        <div class="ribbon ribbon-success">Thông báo thành công</div>
        <p class="mb-0 text-success">{{ $message }}</p>
    </div> <!-- end box-body-->
</div>
@endif

@if ($message = Session::get('error'))
<div class="box">
    <div class="box-body ribbon-box">
        <div class="ribbon ribbon-danger">Thông báo lỗi</div>
        <p class="mb-0 text-danger">{{ $message }}</p>
    </div> <!-- end box-body-->
</div>
@endif

@if ($message = Session::get('warning'))
<div class="box">
    <div class="box-body ribbon-box">
        <div class="ribbon ribbon-warning">Cảnh báo</div>
        <p class="mb-0 text-warning">{{ $message }}</p>
    </div> <!-- end box-body-->
</div>
@endif

@if ($message = Session::get('info'))
<div class="box">
    <div class="box-body ribbon-box">
        <div class="ribbon ribbon-info">Thông tin quan trọng</div>
        <p class="mb-0 text-info">{{ $message }}</p>
    </div> <!-- end box-body-->
</div>
@endif

@if ($errors->any())
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    Check the following errors :(
</div>
@endif