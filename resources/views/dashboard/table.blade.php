@extends('dashboard.app.app')
@section('content')
	<div>
		<button data-bs-toggle="modal" data-bs-target="#addTable" class="waves-effect waves-light btn btn-info mb-5 add-table">Thêm bàn mới</button>
	</div>
	<form class="form-horizontal" action="{{ asset('dashboard/table/addTable') }}" method="post">
		@csrf
		<div id="addTable" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Thêm bàn mới</h4>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>

					<div class="modal-body">					
						<div class="form-group">
							<label class="col-md-12 form-label">Tên Bàn</label>
							<div class="col-md-12">
								<input type="text" class="form-control" placeholder="nhập tên bàn" name="name">
							</div>
						</div>
						<div class="modal-footer">
							
							<button type="submit" class="btn btn-success">Thêm</button>
					
							<button type="button" class="btn btn-danger float-end" data-bs-dismiss="modal">Hủy</button>
						</div>
					</div>
				<!-- /.modal-content -->
				</div>
			<!-- /.modal-dialog -->
			</div>
		</div>
	</form>
	@include('flash-message')

	<div class="box">
						<div class="box-body">
							<h4 class="box-title">Danh sách bàn:</h4>
							<div class="table-responsive">
								<table class="table">
									<thead class="bg-warning">
										<tr>
											<th>Tên Bàn</th>
											<th>Trạng Thái</th>
											<th>Hành Động</th>
										</tr>
									</thead>
									<tbody>
										@foreach($tables as $table)
											<tr>
												<td>{{ $table->name }}</td>
												<td>@if ($table->status == 0)
														<span>Đang Trống</span>
													@elseif ($table->status == 1)
														<div class="text-success">Đang Có Khách</div>
													@elseif ($table->status == 2)
														<div class="text-warning">Đã Có Khách Đặt</div>
												@endif
												</td>
												<td>
													<button data-bs-toggle="modal" data-bs-target="#editTable{{ $table->id }}" value="edit" type="button" class="btn btn-warning" ><i class="fa fa-edit" aria-hidden="true"></i>Sửa</button>
													<button data-bs-toggle="modal" data-bs-target="#deleteTable{{ $table->id }}" value="delete" type="button" class="btn btn-danger" ><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</button>
													
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
<!-- modal sua ban -->
		@foreach ($tables as $table)
				
		<form class="form-horizontal" action="{{ asset('dashboard/table/edit/')}}/{{$table->id }}" method="post">
		@csrf
			<div id="editTable{{ $table->id }}" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="editTable" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="editTable">Sửa {{ $table->name }}</h4>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>

						<div class="modal-body">					
							<div class="form-group">
								<label class="col-md-12 form-label">Tên Bàn:</label>
								<div class="col-md-12">
									<input type="text" class="form-control" value="{{ $table->name }}" placeholder="nhập tên bàn" name="name">
								</div>

								<label class="col-md-12 form-label">Trạng Thái:</label>
								<div class="col-md-12">
									<div class="input-group mb-3">
									  <select class="form-select" id="inputGroupSelect01" name="status">
									    <option value="0">Đang Trống</option>
									    <option value="1">Đang Có Khách</option>
									    <option value="2">Đã Có Khách Đặt</option>
									  </select>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								
								<button type="submit" class="btn btn-success">Thêm</button>
						
								<button type="button" class="btn btn-danger float-end" data-bs-dismiss="modal">Hủy</button>
							</div>
						</div>
					<!-- /.modal-content -->
					</div>
				<!-- /.modal-dialog -->
				</div>
			</div>
		</form>
		@endforeach
<!-- modal xóa bàn -->
@foreach ($tables as $table)
		<form class="form-horizontal" action="{{ asset('dashboard/table/delete/')}}/{{ $table->id }}" method="post">
		@csrf
			<div id="deleteTable{{ $table->id }}" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="deleteTable" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="deleteTable">Có thật sự muốn xóa không?</h4>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">					
								
								<button type="submit" class="btn btn-success">Có</button>
						
								<button type="button" class="btn btn-danger float-end" data-bs-dismiss="modal">Không</button>
						</div>
					<!-- /.modal-content -->
					</div>
				<!-- /.modal-dialog -->
				</div>
			</div>
		</form>
@endforeach

	<!-- Vertically centered modal -->
	{{ $tables->links() }}
@endsection