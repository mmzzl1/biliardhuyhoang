<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.multipurposethemes.com/admin/joblly-admin-template-dashboard/bs5/main-dark/auth_login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 30 May 2021 21:24:41 GMT -->
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://www.multipurposethemes.com/admin/joblly-admin-template-dashboard/bs5/images/favicon.ico">

    <title>Biliard Huy Hoang - Đăng nhập </title>
  
	<!-- Vendors Style-->
	<link rel="stylesheet" href="css/vendors_css.css">
	  
	<!-- Style-->  
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/skin_color.css">	

</head>
	
<body class="hold-transition theme-primary bg-img" style="background-image: url(images/auth-bg/bg-1.jpg)">
	
	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">	
			
			<div class="col-12">
				<div class="row justify-content-center g-0">
					<div class="col-lg-5 col-md-5 col-12">
						<div class="bg-white rounded10 shadow-lg">
							<img src="" alt="">
							<div class="content-top-agile p-20 pb-0">
								<h2 class="text-primary">{{ env("TYPE_SHOP")}} {{env("NAME_SHOP")}}</h2>
								<img class="mb-0" src="">aa
							</div>
							@include('flash-message')
							<div class="p-40">
								<form action="{{asset('/login')}}" method="post">
									@csrf
									<div class="form-group">
										<div class="input-group mb-3">
											<span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
											<input type="text" class="form-control ps-15 bg-transparent" placeholder="Tên đăng nhập" name="name">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group mb-3">
											<span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
											<input type="password" class="form-control ps-15 bg-transparent" placeholder="Mật khẩu" name="password">
										</div>
									</div>
									  <div class="row">
										<div class="col-6">
										</div>
										<!-- /.col -->
										<div class="col-12 text-center">
										  <button type="submit" class="btn btn-success col-12">Đăng nhập</button>
										</div>
										<!-- /.col -->
									  </div>
								</form>	
							</div>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Vendor JS -->
	<script src="js/vendors.min.js"></script>
	<script src="js/pages/chat-popup.js"></script>
    <script src="assets/icons/feather-icons/feather.min.js"></script>	

</body>

<!-- Mirrored from www.multipurposethemes.com/admin/joblly-admin-template-dashboard/bs5/main-dark/auth_login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 30 May 2021 21:24:41 GMT -->
</html>
