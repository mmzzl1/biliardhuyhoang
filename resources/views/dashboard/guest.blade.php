@extends('dashboard.app.app')

@section('content')
<div class="col-sm-12">
	<div class="row">
		
			<div class="d-inline col-sm-6">
				@csrf
				<input type="text" class="form-control m-input" placeholder="Tìm kiếm theo số điện thoại hoặc tên" required="" aria-invalid="false" name="search-guest" id="search-guest">
				</div>
				<div class="d-inline col-sm-6">
						<button class="btn btn-info btn-sm" type="submit">Tìm kiếm</button> 
				</div>

	</div>

<div id="guest-list"><br>
		
</div>
<hr>
	<div class="table-responsive">
		<h4 class="box-title">Danh sách khách hàng:</h4>
		<button data-bs-toggle="modal" data-bs-target="#addGuest" class="waves-effect waves-light btn btn-info mb-10 add-table"><i class="fa fa-user-plus" aria-hidden="true"></i> Thêm khách hàng mới</button>
		<table class="table b-1 border-warning">
			<thead class="bg-warning">
				<tr>
					<th>ID</th>
					<th>Tên</th>
					<th>Số điện thoại</th>
					<th>Địa chỉ</th>
					<th>Thời gian tạo</th>
					<th>Thời gian sửa</th>
				</tr>
			</thead>
			<tbody>
				@foreach($Guests as $Guest)
				<tr>
					<td>{{ $Guest->id }}</td>
					<td>{{ $Guest->name }}</td>
					<td>{{ $Guest->phone }}</td>
					<td>{{ $Guest->address }}</td>
					<td>{{ $Guest->created_at }}</td>
					<td>{{ $Guest->updated_at }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{{ $Guests->links() }}
<!-- modal them khach hang -->
<form action="{{ asset('dashboard/guest/add') }}" method="post">
	@csrf
	<div id="addGuest" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="addGuest" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="addGuest">Thêm khách hàng mới</h4>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">					
					<div class="form-group">
						<label class="col-md-12 form-label">Tên Khách Hàng:</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="nhập tên khách hàng" name="name">
						</div>
						<label class="col-md-12 form-label">Số Điện Thoại:</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="nhập số điện thoại" name="phone">
						</div>
						<label class="col-md-12 form-label">Địa Chỉ:</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="nhập địa chỉ" name="address">
						</div>

					</div>
					<div class="modal-footer">
						
						<button type="submit" class="btn btn-success">Thêm</button>
				
						<button type="button" class="btn btn-danger float-end" data-bs-dismiss="modal">Hủy</button>
					</div>
				</div>
			<!-- /.modal-content -->
			</div>
		<!-- /.modal-dialog -->
		</div>
	</div>
</form>
	
<script>
$(document).ready(function(){

	$('#search-guest').keyup(function(){ //bắt sự kiện keyup khi người dùng gõ từ khóa tim kiếm
	    var keyword = $(this).val(); //lấy gía trị ng dùng gõ
	    if(keyword != '') //kiểm tra khác rỗng thì thực hiện đoạn lệnh bên dưới
	    {
		    var _token = $('input[name="_token"]').val(); // token để mã hóa dữ liệu
		    $.ajax({
		      	url:"{{ asset('/dashboard/search-guest') }}", // đường dẫn khi gửi dữ liệu đi 'search' là tên route mình đặt bạn mở route lên xem là hiểu nó là cái j.
		      	method:"POST", // phương thức gửi dữ liệu.
		      	data:{keyword:keyword, _token:_token},
		      	success:function(data){ //dữ liệu nhận về
			       	$('#guest-list').fadeIn();  
			       	$('#guest-list').html(data); //nhận dữ liệu dạng html và gán vào cặp thẻ có id là countryList
	     		}
   			});
   		}
 });

   $(document).on('click', 'li', function(){  
    $('#search-guest').val($(this).text());  
    $('#guest-list').fadeOut();  
  });  

 });
</script>


@endsection