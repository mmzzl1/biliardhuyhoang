<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://www.multipurposethemes.com/admin/joblly-admin-template-dashboard/bs5/images/favicon.ico">

    <title>{{env("TYPE_SHOP")}} {{env("NAME_SHOP")}}</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Vendors Style-->
	<link rel="stylesheet" href="{{ asset('css/vendors_css.css') }}">
	  
	<!-- Style-->  
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/skin_color.css') }} ">
     
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-primary fixed">
	
<div class="wrapper">	
  <header class="main-header">
	<div class="d-flex align-items-center logo-box justify-content-start">
		<a href="#" class="waves-effect waves-light nav-link d-none d-md-inline-block mx-10 push-btn bg-transparent" data-toggle="push-menu" role="button">
			<i data-feather="menu"></i>
		</a>	
		<!-- Logo -->
		<a href="{{ asset('/dashboard') }}" class="logo">
		  <!-- logo-->
		  <div class="logo-lg">
			  <span class="dark-logo"><img src="{{asset('images/logo.png')}}" alt="logo"></span>
		  </div>
		</a>	
	</div>  
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
	  <div class="app-menu">
		<ul class="header-megamenu nav">
			<li class="btn-group nav-item d-md-none">
				<a href="#" class="waves-effect waves-light nav-link push-btn" data-toggle="push-menu" role="button">
					<i data-feather="menu"></i>
			    </a>
			</li>
		</ul> 
	  </div>
      <div class="navbar-custom-menu r-side">
        <ul class="nav navbar-nav">	
		  <li class="btn-group nav-item d-lg-flex d-none align-items-center">
			<p class="mb-0 text-fade pe-10 pt-5">
				<?php 
					use Carbon\Carbon;
					 $date = Carbon::now('Asia/Ho_Chi_Minh')->locale('vi_VI');

				  	echo $date->isoFormat('LLLL');
				?></p>
		  </li>
		 
        </ul>
      </div>
    </nav>
  </header>  
  
  <aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar position-relative">	
		<div class="user-profile px-20 py-10">
			<div class="d-flex align-items-center">			
				<div class="image">
				  <img src="{{asset('images/avatar/avatar-13.png')}}" class="avatar avatar-lg bg-primary-light rounded100" alt="User Image">
				</div>
				<div class="info">
					<a class="dropdown-toggle px-20" data-bs-toggle="dropdown" href="#">{{Auth::user()->name}}</a>
					<div class="dropdown-menu">

					  <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
					 														document.getElementById('logout-form').submit();"><i data-feather="log-out"></i> log-out</a>
					</div>
					<form id="logout-form" action="{{route('logout')}}" method="post" style="display: none;">
						@csrf
					</form>
				</div>
			</div>
	    </div>
	  	<div class="multinav">
		  	<div class="multinav-scroll" style="height: 100%;">	
			  <!-- sidebar menu-->
			   <ul class="sidebar-menu" data-widget="tree">	
			   	<li class="header">Phục Vụ Nhanh </li>
			   					<li class="treeview">
				  <a href="#">
					<i class="fa fa-shopping-basket" aria-hidden="true"></i>
					<span>Bán Hàng</span>
				  </a>
				</li>
			  	<li class="header">Chức năng ADMIN </li>
			  	<li class="treeview">
				  <a href="#">
				  	<i class="fa fa-gear" aria-hidden="true"></i>
					
					<span>Thiết Lập</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
					<ul class="treeview-menu">
					  	<li><a href="{{ asset('/dashboard')}}"><i class="fa fa-info-circle" aria-hidden="true"></i>Thông tin cửa hàng</a></li>
					  	<li><a href="{{ asset('/dashboard')}}"><i class="fa fa-user-circle" aria-hidden="true"></i>Vai trò tài khoản</a></li>	
					  	<li><a href="{{ asset('dashboard/table') }}"><i class="fa fa-fw fa-table"></i>Thiết lập bàn</a></li>	
				  	</ul>
				</li>

			  	<li class="treeview">
				  <a href="#">
				  	<i class="fa fa-line-chart" aria-hidden="true"></i>
					
					<span>Thống Kê Báo Cáo</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
					<ul class="treeview-menu">
					  	<li><a href="{{ asset('/dashboard')}}"><i class="fa fa-fw fa-refresh"></i>Thống Kê Thu Chi</a></li>
					  	<li><a href="{{ asset('/dashboard')}}"><i class="fa fa-bank" aria-hidden="true"></i>Lợi Nhuận</a></li>	
					  	<li><a href="{{ asset('/dashboard')}}"><i class="fa fa-gavel" aria-hidden="true"></i>Công Nợ Khách Hàng</a></li>	
				  	</ul>
				</li>

				<li class="treeview">
				  <a href="#">
				  	<i class="fa fa-home" aria-hidden="true"></i>
					
					<span>Quản Lý Kho</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
					<ul class="treeview-menu">
						<li><a href="{{ asset('/dashboard/stock')}}"><i class="fa fa-fw fa-list-alt"></i>Kho Hàng</a></li>
				  	</ul>
				</li>

				<li class="treeview">
				  <a href="#">
				  	<i class="fa fa-address-book" aria-hidden="true"></i>
					
					<span>Khách Hàng</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
					<ul class="treeview-menu">
					  	<li><a href="{{ asset('/dashboard/guest') }}"><i class="fa fa-fw fa-list-alt"></i>Danh Sách Khách Hàng</a></li>
				  	</ul>
				</li>

				<li class="treeview">
				  <a href="#">
				  	<i class="fa fa-shopping-bag" aria-hidden="true"></i>
					
					<span>Sản Phẩm</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
					<ul class="treeview-menu">
						<li><a href="{{ asset('/dashboard')}}"><i class="fa fa-telegram" aria-hidden="true"></i>Đăng sản phẩm</a></li>
					  	<li><a href="{{ asset('dashboard/product-categories') }}"><i class="fa fa-sitemap" aria-hidden="true"></i>Phân loại sản phẩm</a></li>
				  	</ul>
				</li>
		  </div>
		</div>
    </section>
  </aside>

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<div class="space"></div>
	  <div class="container-full">
		<!-- Main content -->
		<section class="content">
			
			@yield('content')

		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
		  <li class="nav-item">
			<a class="nav-link" href="javascript:void(0)">FAQ</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="#">Purchase Now</a>
		  </li>
		</ul>
    </div>
	  &copy; 2021 Bản quyền thuộc về {{env("TYPE_SHOP")}} {{env("NAME_SHOP")}}</a>.
  </footer>


</div>
	

	
</body>
	<!-- Vendor JS -->
	<script src="{{ asset('js/vendors.min.js') }}"></script>
	<script src="{{ asset('js/pages/chat-popup.js') }}"></script>
    <script src="{{asset('assets/icons/feather-icons/feather.min.js')}}"></script>
	<script src="{{asset('assets/vendor_components/moment/min/moment.min.js')}}"></script>
	<script src="{{asset('assets/vendor_components/fullcalendar/fullcalendar.js')}}"></script>
	<!-- Joblly App -->
	<script src="{{ asset('js/template.js') }} "></script>
	<script src="{{ asset('js/pages/dashboard.js') }} "></script>
	<script src="{{ asset('js/pages/calendar-dash.js') }} "></script>
</html>