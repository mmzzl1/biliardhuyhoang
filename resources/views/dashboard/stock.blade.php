@extends('layouts.dashboard')
@section('title', @trans('texts.register') . @trans('dashboard.title'))
@section('dashboard.content')
  @if ($errors->all())
    <div class="ui red message">
      @if ($errors->has('aff_code'))
        {{ $errors->first('aff_code') }}
      @else
        <h5 class="ui header">Đăng kí tài khoản thất bại, mời bạn kiểm tra lại các thông tin đã nhập</h5>
      @endif
    </div>
  @endif
  {!! Form::open(['url' => 'register', 'id' => 'registerForm','class' => 'ui form', 'method' => 'POST']) !!}
  <div class="ui piled green segment">
    <h4 class="ui horizontal divider">Đăng kí tài khoản mới</h4>
    @if (Session::get('dashboard-aff_code'))
      <div class="ui info message">
        Đang sử dụng mã giới thiệu:
        <strong>{{ Session::get('dashboard-aff_code')  }}</strong>
      </div>
    @endif
    <div class="ui yellow message">
      <strong>Chú ý:</strong>
      Để đảm bảo tính an toàn cho tài khoản, hãy nhập thật chính xác các thông tin dưới đây.
    </div>
    <div class="field{{ $errors->has('account_name') ? ' error' : ''}}">
      {!! Form::label('account_name', trans('texts.username')) !!}
      <div class="ui left icon input">
        <i class="user icon"></i>
        {!! Form::text('account_name', '', ['placeholder' => trans('texts.username')]) !!}
      </div>
      @if ($errors->has('account_name'))
        <div class="ui basic red pointing prompt label transition visible">
          {{ $errors->first('account_name') }}
        </div>
      @endif
    </div>
    <div class="field{{ $errors->has('password') ? ' error' : '' }}">
      {!! Form::label('password', trans('texts.password')) !!}
      <div class="ui left icon input">
        <i class="lock icon"></i>
        {!! Form::password('password', ['placeholder' => trans('texts.password')]) !!}
      </div>
      <small>Tối thiểu 6 kí tự</small><br />
      <small>Có ít nhất một kí tự viết thường</small><br />
      <small>Có ít nhất một kí tự viết hoa</small><br />
      <small>Có ít nhất một chữ số từ 0-9</small><br />
      <small>Có ít nhất một kí tự đặc biệt (VD: !@#%$%^,)</small><br />
      @if ($errors->has('password'))
        <div class="ui basic red pointing prompt label transition visible">
          {{ $errors->first('password') }}
        </div>
      @endif
    </div>
    <div class="field{{ $errors->has('password_confirmation') ? ' error' : '' }}">
      {!! Form::label('password_confirmation', trans('texts.confirmpassword')) !!}
      <div class="ui left icon input">
        <i class="lock icon"></i>
        {!! Form::password('password_confirmation', ['placeholder' => trans('texts.confirmpassword')]) !!}
      </div>
      @if ($errors->has('password_confirmation'))
        <div class="ui basic red pointing prompt label transition visible">
          {{ $errors->first('password_confirmation') }}
        </div>
      @endif
    </div>
    <div class="field{{ $errors->has('email') ? ' error' : '' }}">
      {!! Form::label('email', trans('texts.email')) !!}
      <div class="ui left icon input">
        <i class="mail icon"></i>
        {!! Form::text('email', '', ['placeholder' => trans('texts.email')]) !!}
      </div>
      @if ($errors->has('email'))
        <div class="ui basic red pointing prompt label transition visible">
          {{ $errors->first('email') }}
        </div>
      @endif
    </div>
    <button class="ui primary button" type="submit">@lang('texts.register')</button>
  </div>
  {!! Form::close() !!}
   <script type="text/javascript">
    $('#registerForm').form({
      fields : {
        account_name: ['empty', 'maxLength[50]'],
        email: ['empty','email','maxLength[255]'],
        password: ['empty','minLength[6]'],
        password_confirmation: ['match[password]'],
      },
      inline : true,
      on     : 'submit'
    });
  </script>
@endsection
