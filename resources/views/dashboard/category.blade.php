@extends('dashboard.app.app')
@section('content')

	<div class="box">
		<div class="box-body">
			<h4 class="box-title">Danh mục sản phẩm: </h4>
			<h6 class="box-subtitle mb-20"><button data-bs-toggle="modal" data-bs-target="#addCategory" class="waves-effect waves-light btn btn-info mb-5">Thêm Danh Mục</button></h6>
			@include('flash-message')
			<div class="table-responsive">
				<table class="table">
					<thead class="bg-warning">
						<tr>
							<th>Tên Danh Mục</th>
							<th>Hành Động</th>
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $category)
							<tr>
								<td>{{ $category->name}}</td>
								<td>
									<button data-bs-toggle="modal" data-bs-target="#editCategory{{ $category->id }}" value="edit" type="button" class="btn btn-warning" ><i class="fa fa-edit" aria-hidden="true"></i>Sửa</button>
									<button data-bs-toggle="modal" data-bs-target="#deleteCategory{{ $category->id }}" value="delete" type="button" class="btn btn-danger" ><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</button>
								</td>
							</td>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- add-category -->
	<form class="form-horizontal" action="{{ asset('dashboard/product-categories/add') }}" method="post">
		@csrf
		<div id="addCategory" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="addCategory" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="addCategory">Thêm Danh Mục Mới</h4>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>

					<div class="modal-body">					
						<div class="form-group">
							<label class="col-md-12 form-label">Tên Danh Mục</label>
							<div class="col-md-12">
								<input type="text" class="form-control" placeholder="nhập tên danh mục" name="name">
							</div>
						</div>
						<div class="modal-footer">
							
							<button type="submit" class="btn btn-success">Thêm</button>
					
							<button type="button" class="btn btn-danger float-end" data-bs-dismiss="modal">Hủy</button>
						</div>
					</div>
				<!-- /.modal-content -->
				</div>
			<!-- /.modal-dialog -->
			</div>
		</div>
	</form>

	<!-- sua danh muc-->
	@foreach ($categories as $category)
				
		<form class="form-horizontal" action="{{ asset('dashboard/product-categories/edit/')}}/{{$category->id }}" method="post">
		@csrf
			<div id="editCategory{{ $category->id }}" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="editTable" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="editTable">Sửa {{ $category->name }}</h4>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>

						<div class="modal-body">					
							<div class="form-group">
								<label class="col-md-12 form-label">Tên Bàn:</label>
								<div class="col-md-12">
									<input type="text" class="form-control" value="{{ $category->name }}" placeholder="nhập tên bàn" name="name">
								</div>								
							</div>
							<div class="modal-footer">
								
								<button type="submit" class="btn btn-success">Thêm</button>
						
								<button type="button" class="btn btn-danger float-end" data-bs-dismiss="modal">Hủy</button>
							</div>
						</div>
					<!-- /.modal-content -->
					</div>
				<!-- /.modal-dialog -->
				</div>
			</div>
		</form>
		@endforeach

	<!-- xoa danh muc-->

		@foreach ($categories as $category)
		<form class="form-horizontal" action="{{ asset('dashboard/product-categories/delete/')}}/{{ $category->id }}" method="post">
		@csrf
			<div id="deleteCategory{{ $category->id }}" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="deleteTable" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="deleteTable">Có thật sự muốn xóa không?</h4>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">					
								
								<button type="submit" class="btn btn-success">Có</button>
						
								<button type="button" class="btn btn-danger float-end" data-bs-dismiss="modal">Không</button>
						</div>
					<!-- /.modal-content -->
					</div>
				<!-- /.modal-dialog -->
				</div>
			</div>
		</form>
@endforeach
{{ $categories->links() }}
@endsection