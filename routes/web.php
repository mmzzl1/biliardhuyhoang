<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'Admin\UserController@index');
Route::post('/login', 'Admin\UserController@login')->name('login');

Route::get('/', 'Admin\UserController@index');



Route::middleware(['auth'])->group(function () {
	Route::prefix('dashboard')->group(function () {
    	Route::get('/', 'Admin\AdminController@index');
	    Route::post('/logout', 'Admin\UserController@logout')->name('logout');
	    //guest
	    Route::post('/search-guest' , 'Admin\GuestController@search');
	    Route::get('/guest', 'Admin\GuestController@index');
	   	Route::post('/guest/add' , 'Admin\GuestController@addGuest');
	   	//table
	    Route::get('/table' , 'Admin\TableController@index');
	    Route::post('/table/addTable' , 'Admin\TableController@addTable');
	    Route::post('/table/edit/{id}' , 'Admin\TableController@editTable');
	    Route::post('/table/delete/{id}' , 'Admin\TableController@deleteTable');
	    //product
	    Route::get('/product-categories' , 'Admin\CategoryController@index');
	    Route::post('/product-categories/add' , 'Admin\CategoryController@addCategory');
	    Route::post('/product-categories/edit/{id}' , 'Admin\CategoryController@editCategory');
	    Route::post('/product-categories/delete/{id}' , 'Admin\CategoryController@deleteCategory');
	    //stock
	    Route::get('/stock' , 'Admin\StockController@index');
	    Route::post('/stock/add' , 'Admin\StockController@addStock');

	});
});